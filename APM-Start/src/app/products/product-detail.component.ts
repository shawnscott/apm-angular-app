import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Product } from './product';
import { ProductService } from './product.service';

@Component({
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {

  pageTitle: string = 'Product Detail';
  product: Product;
  errorMessage: string;
  imageWidth: number = 250;

  constructor(
    private route: ActivatedRoute, 
    private productService: ProductService,
    private router: Router) {}

  ngOnInit() {
    let id = +this.route.snapshot.params.id;
    
    this.productService.getProduct(id).subscribe({
      next: products => this.product = products[0],
      error: err => this.errorMessage = err
    });
  }

  onBack(): void {
    this.router.navigate(['/products']);
  }

}
