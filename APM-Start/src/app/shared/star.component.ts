import { Component, OnChanges, Input, EventEmitter, Output } from '@angular/core';

@Component({
    selector: 'pm-star',
    templateUrl: './star.component.html',
    styleUrls: ['./star.component.css']
})
export class StarComponent implements OnChanges {

    @Input() rating: number;
    starWidth: number;
    @Output() ratingClicked: EventEmitter<string> = new EventEmitter<string>();
    starRange: number[] = [1,2,3,4,5];

    constructor() {}
    ngOnChanges() {
        this.starWidth = this.rating * 75 / 5;
    }

    onClick(rating: number) {
        this.ratingClicked.emit(`Your rating is ${rating}`);
        this.rating = rating;
    }

    onEnter(rating?: number) {
        this.starWidth = rating * 75 / 5;
    }

    onExit() {
        this.starWidth = this.rating * 75 / 5;
    }

}